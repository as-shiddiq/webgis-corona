# WebGIS Penyebaran Virus Corona
Aplikasi WebGIS Penyebaran Virus Corona di Dunia, ditujukan untuk pembelajaran
![WebGIS Penyebaran Virus Corona](ss.png "WebGIS Penyebaran Virus Corona")

# Komponen dalam membangun Aplikasi
1. Template Menggunakan [Bootstrap 4](https://getbootstrap.com/)
2. Javascript Library Menggunakan [JQuery](https://jquery.com/)
3. Library GIS menggunkaan [Leaflet](https://leafletjs.com/)
4. Data GIS mengggunakan (https://gisanddata.maps.arcgis.com/apps/opsdashboard/index.html#/bda7594740fd40299423467b48e9ecf6)

# Donasi
Jika dirasa Coding yang aku bagi tersebut bermanfaat, tidak salahnya bagi kalian jika ingin berdonasi. 
untuk donasi kalian bisa melalui : 
__BANK BRI__\
__No. Rekening 023901023305509__\
__An. Nasrullah Sidik__
atau 
https://trakteer.id/as.shiddiq

Berapapun Donasi Anda, Saya ucapkan Terima kasih banyak :)

Copyright © 2020 [Nasrullah Siddik](bit.ly/YTNSiddik). All rights reserved.



<?php
header("Access-Control-Allow-Origin: *");
$url='http://covid19.tanahlautkab.go.id/';
$data=file_get_contents($url,TRUE);
function parseTable($html)
{
  preg_match("/<table.*?>.*?<\/[\s]*table>/s", $html, $table_html);
  preg_match_all("/<th.*?>(.*?)<\/[\s]*th>/", $table_html[0], $matches);
  $row_headers = $matches[1];
  preg_match_all("/<tr.*?>(.*?)<\/[\s]*tr>/s", $table_html[0], $matches);
  $table = array();
  foreach($matches[1] as $row_html)
  {
    preg_match_all("/<td.*?>(.*?)<\/[\s]*td>/", $row_html, $td_matches);
    $row = array();
    for($i=0; $i<count($td_matches[1]); $i++)
    {
      $td = strip_tags(html_entity_decode($td_matches[1][$i]));
      if($i==0){
        $row_headers[$i]='Kecamatan';
      }
      elseif($i==4){
        $row_headers[$i]='Positif';
      }
      $row[$row_headers[$i]] = trim($td);
    }
    if(count($row) > 0)
      $table[] = $row;
  }
  return $table;
}
$output = parseTable($data);

$response;
foreach ($output as $key => $value) {
	$response[]=$value;
}
header('Content-type: application/json');

echo json_encode($response);
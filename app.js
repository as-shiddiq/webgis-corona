var table = $('.table-indonesia').DataTable({
	"bJQueryUI": false,
          "bAutoWidth": false,
          "aaSorting": [[1, "desc"]],
          "stateSave": true,
          "iDisplayLength": 10, 
          "processing": true,
            "aLengthMenu": [
              [5,10, 30, 50, -1],
              [5,10, 30, 50, "All"]
            ],
	      "order": [[ 0, "asc" ]],
          "language": {
                zeroRecords: "Maaf data tidak ditemukan",
                info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                infoEmpty: "Menampilkan 0 sampai 0 dari 0 data",
                infoFiltered: "(Disaring dari _MAX_ total data)",
                searchPlaceholder: 'Cari...',
                search: '',
                lengthMenu: '_MENU_ Item/Halaman',
          },
          responsive: true
});
var tabletala = $('.table-tala').DataTable({
	"bJQueryUI": false,
          "bAutoWidth": false,
          "aaSorting": [[1, "desc"]],
          "stateSave": true,
          "iDisplayLength": 10, 
          "processing": true,
            "aLengthMenu": [
              [5,10, 30, 50, -1],
              [5,10, 30, 50, "All"]
            ],
	      "order": [[ 0, "asc" ]],
          "language": {
                zeroRecords: "Maaf data tidak ditemukan",
                info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                infoEmpty: "Menampilkan 0 sampai 0 dari 0 data",
                infoFiltered: "(Disaring dari _MAX_ total data)",
                searchPlaceholder: 'Cari...',
                search: '',
                lengthMenu: '_MENU_ Item/Halaman',
          },
          responsive: true
});
var tabledunia = $('.table-dunia').DataTable({
	"bJQueryUI": false,
          "bAutoWidth": false,
          "aaSorting": [[1, "desc"]],
          "stateSave": true,
          "iDisplayLength": 10, 
          "processing": true,
            "aLengthMenu": [
              [5,10, 30, 50, -1],
              [5,10, 30, 50, "All"]
            ],
	      "order": [[ 0, "asc" ]],
          "language": {
                zeroRecords: "Maaf data tidak ditemukan",
                info: "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                infoEmpty: "Menampilkan 0 sampai 0 dari 0 data",
                infoFiltered: "(Disaring dari _MAX_ total data)",
                searchPlaceholder: 'Cari...',
                search: '',
                lengthMenu: '_MENU_ Item/Halaman',
          },
          responsive: true
});
var totalConfirmed=0;
var totalRecovered=0;
var markerindonesia=[];
var totalDeaths=0;
var map = L.map('map',{
		center:[-3.824181, 114.8191513],
		zoom:3,
		fullscreenControl: {
	    	pseudoFullscreen: false,
	}
});
var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
		'<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	mbUrl = 'https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
var light   = L.tileLayer(mbUrl, {id: 'mapbox.light', attribution: mbAttr}),
	streets  = L.tileLayer(mbUrl, {id: 'mapbox.streets', attribution: mbAttr}),
	dark = L.tileLayer(mbUrl, {id: 'mapbox.dark', attribution: mbAttr})
var baseLayers={
	'Light':light,
	'Dark Mode':dark,
	'Streets':streets,
};

$(document).on("click",'.map-zoom',function(){
	var lat=$(this).data('lat');
	var lng=$(this).data('lng');
	var zoom=$(this).data('zoom');
  	map.setZoom(zoom);
  	map.panTo(new L.LatLng(lat, lng));
})
$(document).on("click",'.map-markerpopup',function(){
	var marker=$(this).data('marker');
	console.log(markerindonesia[marker].openPopup())
})

function getColor(percent) {
	var color='#f03';
	if(percent>=90){
		color='#090'
	}
	else if(percent>=80){
		color='#009984'
	}
	else if(percent>=70){
		color='#949'
	}
	else if(percent>=60){
		color='#049'
	}
	else if(percent>=50){
		color='#f90'
	}
	return color;
}


function setfillColor(confirmed,recovered){
	var percent=(recovered/confirmed*100);
	return getColor(percent);
	
}


getData();
var markersdunia = L.layerGroup();
var markersindonesia = L.layerGroup();

// ajax ambil data json
function getData(){
	$.ajax({
		url:"https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/ncov_cases/FeatureServer/1/query?f=json&where=1%3D1&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=Confirmed%20desc%2CCountry_Region%20asc%2CProvince_State%20asc&outSR=102100&resultOffset=0&resultRecordCount=250&cacheHint=true",
		dataType:"JSON",
		success:function(data){
			var features=data.features;
			for (var i =0; i< features.length ; i++) {
				var attributes=features[i].attributes;
				// console.log(attributes);
				totalRecovered+=attributes.Recovered;
				totalDeaths+=attributes.Deaths;
				totalConfirmed+=attributes.Confirmed;
				if(attributes.Lat!=null && attributes.Long_!=null){
					var marker=L.circleMarker([attributes.Lat,attributes.Long_],{
						weight:1,
						color:"#5554",
						fillColor:setfillColor(attributes.Confirmed,attributes.Recovered),
						fillOpacity:0.7,
						radius: radius(attributes.Confirmed)
					});
					
					marker.bindPopup(
							"<h4>OBJECTID: "+attributes.OBJECTID+"</h4>"+
							"<table>"+
							"<tr>"+
							"<td>Negara</td><td>:</td><td>"+attributes.Country_Region+"</td>"+
							"</tr>"+
							"<tr>"+
							"<td>Provinsi</td><td>:</td><td>"+attributes.Province_State+"</td>"+
							"</tr>"+
							"<tr>"+
							"<td>Terinfeksi</td><td>:</td><td>"+attributes.Confirmed+"</td>"+
							"</tr>"+
							"<tr>"+
							"<td>Meninggal</td><td>:</td><td>"+attributes.Deaths+"</td>"+
							"</tr>"+
							"<tr>"+
							"<td>Sembuh</td><td>:</td><td>"+attributes.Recovered+"</td>"+
							"</tr>"+
							"</table>"+
							"<div class='footer'><a href=# class='map-zoom' data-zoom='6' data-lat='"+attributes.Lat+"' data-lng='"+attributes.Long_+"'><i class='fa fa-search-plus'></i> Zoom In</a></div>"
						,{className:'popup-panel'});
					marker.addTo(markersdunia);

					marker.on('mouseover',function(e){
						var marker = e.target;
						marker.setStyle({
							color: 'white',
							dashArray: '3'
						});
					});
					marker.on('mouseout',function(e){
						var marker = e.target;
						marker.setStyle({
							color:"#5554",
							dashArray: '1'
						});
					});
				}
			}
			$("#nav-dunia .total-confirmed h4").append(totalConfirmed.toLocaleString());

			$("#nav-data-dunia .id-terkonfirmasi h4").append(totalConfirmed.toLocaleString());
			$("#nav-data-dunia .id-sembuh h4").append(totalRecovered.toLocaleString());
			$("#nav-data-dunia .id-meninggal h4").append(totalDeaths.toLocaleString());
			$("#nav-data-dunia .persen-meninggal").append((totalDeaths/totalConfirmed*100).toFixed(2));
			$("#nav-data-dunia .persen-sembuh").append((totalRecovered/totalConfirmed*100).toFixed(2));

			var legendpersentase = L.control({position: 'bottomright'});
			legendpersentase.onAdd = function (map) {
				var div = L.DomUtil.create('div', 'info legend'),
					grades = [0, 50, 60, 70, 80, 90],
					labels = [],
					from, to;

				labels.push('<strong>Informasi Peta</strong>');
				labels.push("<ul class='list-group'>");
				for (var i = (grades.length-1); i >= 0; i--) {
					from = grades[i];
					to=grades[i+1];

					if(i==grades.length-1){
					labels.push('<li class="list-group-item"><i style="background:' + getColor(from) + '"></i>'+from+'% Sembuh</li>');
					}
					else if(i==0){
						labels.push('<li class="list-group-item"><i style="background:' + getColor(from) + '"></i>< '+grades[i+1]+'% Sembuh</li>');
					}
					else{
					labels.push('<li class="list-group-item"><i style="background:' + getColor(from) + '"></i>'+from+'% - '+to+' % Sembuh</li>');
					}
				}
				labels.push("</ul>");
				div.innerHTML = labels.join('');
				return div;
			};

			legendpersentase.addTo(map);


		}
	});
	$.ajax({
		url:"https://services1.arcgis.com/0MSEUqKaxRlEPj5g/arcgis/rest/services/ncov_cases/FeatureServer/2/query?f=json&where=Confirmed%20%3E%200&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=Confirmed%20desc&resultOffset=0&resultRecordCount=200&cacheHint=true",
		dataType:"JSON",
		success:function(dataJson){
			var features=dataJson.features;
			var delay=0;
			for (var i =0; i< features.length ; i++) {
				var attributes=features[i].attributes;
				var percent=(attributes.Recovered/attributes.Confirmed*100).toFixed(2);
				delay=(i*0.1);
				if(i>10){
					delay=1;
				}
				$("#nav-dunia .list-confirmed").append("<li class='list-group-item map-zoom animated flipInX' style='animation-delay: "+delay+"s' data-lat='"+attributes.Lat+"' data-zoom='6' data-lng='"+attributes.Long_+"'>"+
					"<span style='float:left;font-size:40px;margin-right:5px;color:#dc3545;margin-top:-10px'><strong>"+(i+1)+"</strong></span>"+
					"<strong>"+attributes.Country_Region+"</strong><span style='margin-left:5px;font-size:14px;color:"+getColor(percent)+"'>["+percent+"%]</span>"+
					"<small>"+
					" <i class='fa fa-check text-warning'></i> "+abbreviate(attributes.Confirmed, 2, false, false)+
					" - <i class='fa fa-smile-o text-success'></i> "+abbreviate(attributes.Recovered, 2, false, false)+
					" - <i class='fa fa-frown-o text-danger'></i> "+abbreviate(attributes.Deaths, 2, false, false)+" ("+(attributes.Deaths/attributes.Confirmed*100).toFixed(2)+"%)"+
					"</small></li>");

				tabledunia.row.add( [ 
				  (i+1),
				  attributes.Country_Region,
				  attributes.Confirmed.toLocaleString(),
				  attributes.Recovered.toLocaleString(),
				  attributes.Deaths.toLocaleString(),
				] )
				.draw();
			}
		}
	});

	$.ajax({
		url:'https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/Statistik_Perkembangan_COVID19_Indonesia/FeatureServer/0/query?f=json&where=Jumlah_Kasus_Kumulatif>0&returnGeometry=false&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=Tanggal%20asc&resultOffset=0&resultRecordCount=2000&cacheHint=true',
		dataType:'JSON',
		success:function(data){
			var features=data.features;
			// console.log(features);
			for(var i = 0 ; i<features.length;i++){
				var attributes=features[i].attributes;
				if(i==(features.length-1)){
					// var lastUpdatedAt=data.metadata.lastUpdatedAt;
					$("#nav-data-indonesia .id-terkonfirmasi h4").append(attributes.Jumlah_Kasus_Kumulatif.toLocaleString());
					$("#nav-data-indonesia .id-dirawat h4").append(attributes.Jumlah_pasien_dalam_perawatan.toLocaleString());
					$("#nav-data-indonesia .id-sembuh h4").append(attributes.Jumlah_Pasien_Sembuh.toLocaleString());
					$("#nav-data-indonesia .id-meninggal h4").append(attributes.Jumlah_Pasien_Meninggal.toLocaleString());
					$("#nav-data-indonesia .persen-meninggal").append((attributes.Jumlah_Pasien_Meninggal/attributes.Jumlah_Kasus_Kumulatif*100).toFixed(2));
					$("#nav-data-indonesia .persen-sembuh").append((attributes.Jumlah_Pasien_Sembuh/attributes.Jumlah_Kasus_Kumulatif*100).toFixed(2));
					$("#nav-data-indonesia .persen-rawat").append((attributes.Jumlah_pasien_dalam_perawatan/attributes.Jumlah_Kasus_Kumulatif*100).toFixed(2));
					$("#nav-indonesia .total-confirmed h4").append(attributes.Jumlah_Kasus_Kumulatif.toLocaleString());

					// Create a new JavaScript Date object based on the timestamp
					// multiplied by 1000 so that the argument is in milliseconds, not seconds.
					var date = new Date(attributes.Tanggal);
					$("#nav-data-indonesia .updated-at").text(date.toString());
				}
		 
			}

		}
	});
	$.ajax({
		url:'https://services5.arcgis.com/VS6HdKS0VfIhv8Ct/arcgis/rest/services/COVID19_Indonesia_per_Provinsi/FeatureServer/0/query?f=geojson&where=(Kasus_Posi%20%3C%3E%200)%20AND%20(Provinsi%20%3C%3E%20%27Indonesia%27)&returnGeometry=true&spatialRel=esriSpatialRelIntersects&outFields=*&orderByFields=Kasus_Posi%20desc&resultOffset=0&resultRecordCount=34&cacheHint=true',
		dataType:'JSON',
		success:function(data){
			var features=data.features;
			// console.log(features);
			var delay=0;
			for(var i = 0 ; i<features.length;i++){
				var attributes=features[i].properties;
				var geometry=features[i].geometry.coordinates;
				delay=(i*0.1);
				if(i>10){
					delay=1;
				}
				// console.log(geometry[0]);
				//conver
					markerindonesia[i]=L.circleMarker([geometry[1],geometry[0]],{
					weight:1,
					color:"#5554",
					fillColor:setfillColor(attributes.Kasus_Posi,attributes.Kasus_Semb),
					fillOpacity:0.7,
					radius: radius(attributes.Kasus_Posi)
				});
				
				markerindonesia[i].bindPopup(
						"<h4>OBJECTID: "+attributes.FID+"</h4>"+
						"<table>"+
						"<td>Provinsi</td><td>:</td><td>"+attributes.Provinsi+"</td>"+
						"</tr>"+
						"<tr>"+
						"<td>Terinfeksi</td><td>:</td><td>"+attributes.Kasus_Posi+"</td>"+
						"</tr>"+
						"<tr>"+
						"<td>Meninggal</td><td>:</td><td>"+attributes.Kasus_Meni+"</td>"+
						"</tr>"+
						"<tr>"+
						"<td>Sembuh</td><td>:</td><td>"+attributes.Kasus_Semb+"</td>"+
						"</tr>"+
						"</table>"+
						"<div class='footer'><a href=# class='map-zoom' data-zoom='12' data-lat='"+geometry[1]+"' data-lng='"+geometry[0]+"'><i class='fa fa-search-plus'></i> Zoom In</a></div>"
					,{className:'popup-panel'});
				markerindonesia[i].addTo(markersindonesia);

				markerindonesia[i].on('mouseover',function(e){
					var marker = e.target;
					marker.setStyle({
						color: 'white',
						dashArray: '3'
					});
				});
				markerindonesia[i].on('mouseout',function(e){
					var marker = e.target;
					marker.setStyle({
						color:"#5554",
						dashArray: '1'
					});
				});
				// tambah data ke datatable

				table.row.add( [ 
				  attributes.FID,
				  attributes.Provinsi,
				  attributes.Kasus_Posi.toLocaleString(),
				  attributes.Kasus_Semb.toLocaleString(),
				  attributes.Kasus_Meni.toLocaleString(),
				] )
				.draw();
				// tambah data ke list
				var percent=(attributes.Kasus_Semb/attributes.Kasus_Posi*100).toFixed(2);

				$("#nav-indonesia .list-confirmed").append("<li class='list-group-item map-zoom map-markerpopup animated flipInX' style='animation-delay: "+delay+"s' data-marker="+i+"  data-lat='"+geometry[1]+"' data-zoom='12' data-lng='"+geometry[0]+"'>"+
					"<span style='float:left;font-size:40px;margin-right:5px;color:#dc3545;margin-top:-10px'><strong>"+(i+1)+"</strong></span>"+
					"<strong>"+attributes.Provinsi+"</strong><span style='margin-left:5px;font-size:14px;color:"+getColor(percent)+"'>["+percent+"%]</span>"+
					"<small>"+
					" <i class='fa fa-check text-warning'></i> "+abbreviate(attributes.Kasus_Posi, 2, false, false)+
					" - <i class='fa fa-smile-o text-success'></i> "+abbreviate(attributes.Kasus_Semb, 2, false, false)+
					" - <i class='fa fa-frown-o text-danger'></i> "+abbreviate(attributes.Kasus_Meni, 2, false, false)+" ("+(attributes.Kasus_Meni/attributes.Kasus_Posi*100).toFixed(2)+"%)"+
					"</small></li>");
			}

		}
	});

	$.ajax({
		url:'tanahlaut-data.php',
		dataType:'JSON',
		success:function(data){
			console.log(data);
			var talaConfirmed=0;
			var talaRecovered=0;
			var talaDeath=0;
			var talaODP=0;
			var talaPDP=0;
			var delay=0;
			for(var i = 0 ; i<(data.length-1);i++){
				delay=(i*0.1);
				if(i>10){
					delay=1;
				}
				tabletala.row.add( [ 
				  (i+1),
				  data[i].Kecamatan,
				  data[i].ODP.toLocaleString(),
				  data[i].PDP.toLocaleString(),
				  data[i].Positif.toLocaleString(),
				  data[i].Sembuh.toLocaleString(),
				  data[i].Meninggal.toLocaleString()
				] )
				.draw();

				var percent;
				if(data.Positif>0 && data.Sembuh>0){
					percent=(data.Sembuh/data.Positif*100).toFixed(2);
				}
				else{
					percent=100;
				}
				$("#nav-tala .list-confirmed").append("<li class='list-group-item animated flipInX' style='animation-delay: "+delay+"s'>"+
					"<span style='float:left;font-size:40px;margin-right:5px;color:#dc3545;margin-top:-10px'><strong>"+(i+1)+"</strong></span>"+
					"<strong>"+data[i].Kecamatan+"</strong><span style='margin-left:5px;font-size:14px;color:"+getColor(percent)+"'>["+percent+"%]</span>"+
					"<small>"+
					" <span class='text-primary'>ODP</span> "+abbreviate(data[i].ODP, 2, false, false)+
					" - <span class='text-primary'>PDP</span> "+abbreviate(data[i].PDP, 2, false, false)+
					" - <i class='fa fa-check-o text-warning'></i> "+abbreviate(data[i].Positif, 2, false, false)+
					" - <i class='fa fa-smile-o text-success'></i> "+abbreviate(data[i].Sembuh, 2, false, false)+
					" - <i class='fa fa-frown-o text-danger'></i> "+abbreviate(data[i].Meninggal, 2, false, false)+
					"</small></li>");
				talaConfirmed+=parseInt(data[i].Positif);
				talaRecovered+=parseInt(data[i].Sembuh);
				talaDeath+=parseInt(data[i].Meninggal);
				talaODP+=parseInt(data[i].ODP);
				talaPDP+=parseInt(data[i].PDP);
			}

			$("#nav-data-tala .id-terkonfirmasi h4").append(talaConfirmed.toLocaleString());
			$("#nav-data-tala .id-pdp h4").append(talaPDP.toLocaleString());
			$("#nav-data-tala .id-odp h4").append(talaODP.toLocaleString());
			$("#nav-data-tala .id-sembuh h4").append(talaRecovered.toLocaleString());
			$("#nav-data-tala .id-meninggal h4").append(talaDeath.toLocaleString());

			$("#nav-data-tala .persen-meninggal").append(talaConfirmed>0?(talaDeath/talaConfirmed*100).toFixed(2):'0');
			$("#nav-data-tala .persen-sembuh").append(talaConfirmed>0?(talaRecovered/talaConfirmed*100).toFixed(2):'100');
			$("#nav-tala .total-confirmed h4").append(talaConfirmed);

		}
	});

	// groupping




}
function abbreviate(number, maxPlaces, forcePlaces, forceLetter) {
  number = Number(number)
  forceLetter = forceLetter || false
  if(forceLetter !== false) {
    return annotate(number, maxPlaces, forcePlaces, forceLetter)
  }
  var abbr
  if(number >= 1e12) {
    abbr = 'T'
  }
  else if(number >= 1e9) {
    abbr = 'B'
  }
  else if(number >= 1e6) {
    abbr = 'M'
  }
  else if(number >= 1e3) {
    abbr = 'K'
  }
  else {
    abbr = ''
  }
  return annotate(number, maxPlaces, forcePlaces, abbr)
}

function annotate(number, maxPlaces, forcePlaces, abbr) {
  // set places to false to not round
  var rounded = 0
  switch(abbr) {
    case 'T':
      rounded = number / 1e12
      break
    case 'B':
      rounded = number / 1e9
      break
    case 'M':
      rounded = number / 1e6
      break
    case 'K':
      rounded = number / 1e3
      break
    case '':
      rounded = number
      break
  }
  if(maxPlaces !== false) {
    var test = new RegExp('\\.\\d{' + (maxPlaces + 1) + ',}$')
    if(test.test(('' + rounded))) {
      rounded = rounded.toFixed(maxPlaces)
    }
  }
  if(forcePlaces !== false) {
    rounded = Number(rounded).toFixed(forcePlaces)
  }
  return rounded + abbr
}

function radius(value){
	var radius;
	if(value>130000){
		radius=48;
	}
	if(value>120000){
		radius=46;
	}
	else if(value>110000){
		radius=44;
	}
	else if(value>100000){
		radius=42;
	}
	else if(value>90000){
		radius=38;
	}
	else if(value>80000){
		radius=36;
	}
	else if(value>70000){
		radius=34;
	}
	else if(value>60000){
		radius=32;
	}
	else if(value>50000){
		radius=30;
	}
	else if(value>40000){
		radius=28;
	}
	else if(value>30000){
		radius=26;
	}
	else if(value>20000){
		radius=24;
	}
	else if(value>10000){
		radius=22;
	}
	else if(value>5000){
		radius=20;
	}
	else if(value>2000){
		radius=18;
	}
	else if(value>1000){
		radius=16;
	}
	else if(value>500){
		radius=14;
	}
	else if(value>200){
		radius=12;
	}
	else if(value>100){
		radius=10;
	}
	else if(value>25){
		radius=8;
	}
	else if(value>15){
		radius=6;
	}
	else if(value>10){
		radius=4;
	}
	else{
		radius=2;
	}
	return radius;
}
var spreads = {
	"Dunia": markersdunia,
	"Indonesia": markersindonesia,
};
map.addLayer(light);
map.addLayer(markersdunia);
map.addLayer(markersindonesia);

L.control.layers(baseLayers,spreads).addTo(map);

			